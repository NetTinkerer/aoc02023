import sys
from collections import defaultdict
with open(sys.argv[1]) as f:
  data = f.read().splitlines()

task1 = 0
task2 = 0
max_values = {"red": 12, "green": 13, "blue": 14}
for game in data:
  draws = defaultdict(list)
  h,game_data = game.split(": ")
  _, game_id = h.split(" ")
  for i in game_data.split("; "):
     j = i.split(", ")
     for k in j:
       no, color = k.split(' ')
       draws[color].append(int(no))
  valid = True
  product = 1
  for l in max_values.keys():
    if max(draws[l]) > max_values[l]:
      valid = False
    product *= max(draws[l])
  if valid:
    task1 += int(game_id)
  task2 += product
  

print(f"task1 {task1}")
print(f"task2 {task2}")
      
  
    
  
 
