import sys
with open(sys.argv[1]) as f:
  data =  f.read().splitlines()

total = 0
numbers2 = {"1":1,
           "2":2,
           "3":3,
           "4":4,
           "5":5,
           "6":6,
           "7":7,
           "8":8,
           "9":9,
           "one": 1,
           "two": 2,
           "three": 3,
           "four": 4,
           "five": 5,
           "six": 6,
           "seven": 7,
           "eight": 8,
           "nine": 9
          }
numbers1 = {"1":1,
           "2":2,
           "3":3,
           "4":4,
           "5":5,
           "6":6,
           "7":7,
           "8":8,
           "9":9
          }
for line in data:
  digits = ""
  lowest_index=1e9
  highest_index=1e9
  for number in numbers1.keys():
    try:
      index = line.index(number)
    except ValueError:
      index = 1e9
    if index < lowest_index:
      lowest_index = index
      val_1 = numbers1[number]
    try:
      index = line[::-1].index(number[::-1])
    except ValueError:
      index = 1e9
    if index < highest_index:
      highest_index = index
      val_2 = numbers1[number]
  total +=(val_1*10+val_2)
print(total)
total=0
for line in data:
  lowest_index=1e9
  highest_index=1e9
  for number in numbers2.keys():
    try:
      index = line.index(number)
    except ValueError:
      index = 1e9
    if index < lowest_index:
      lowest_index = index
      val_1 = numbers2[number]
    try:
      index = line[::-1].index(number[::-1])
    except ValueError:
      index = 1e9
    if index < highest_index:
      highest_index = index
      val_2 = numbers2[number]
  total +=(val_1*10+val_2)
print(total)
