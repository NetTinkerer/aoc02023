import sys
from pprint import pprint
from collections import defaultdict, Counter
with open(sys.argv[1]) as f:
  data = f.read().splitlines()
scoring ="A, K, Q, J, T, 9, 8, 7, 6, 5, 4, 3, 2"
scores = {j:i+1 for i,j in enumerate(scoring.split(', ')[::-1])} 
scoring_joker ="A, K, Q, T, 9, 8, 7, 6, 5, 4, 3, 2, J"
scores_joker = {j:i+1 for i,j in enumerate(scoring_joker.split(', ')[::-1])} 
hands = {}
for line in data:
  cards,bet = line.split()
  hands[cards]={"bet": int(bet)}
  hands[cards]["cards"] = Counter([i for i in cards])
  hands[cards]["multiples"] = Counter([i for i in hands[cards]['cards'].values()])
  hands[cards]["score"] = int("".join([str(scores[i]).zfill(2) for i in cards]))
  hands[cards]["score_joker"] = int("".join([str(scores_joker[i]).zfill(2) for i in cards]))

hands_ranking = [i for i in hands.keys()]

hands_per_type = defaultdict(list)
hands_per_type_joker = defaultdict(list)
for key,hand in hands.items():
  if 5 in hand['multiples']:
    hands_per_type["5_of_a_kind"].append(key)
    hands_per_type_joker["5_of_a_kind"].append(key)
  elif 4 in hand['multiples']:
    hands_per_type["4_of_a_kind"].append(key)
    if hand["cards"]["J"] in [1, 4]:
      hands_per_type_joker["5_of_a_kind"].append(key)
    else:
      hands_per_type_joker["4_of_a_kind"].append(key)
  elif all([3 in hand['multiples'],2 in hand['multiples']]):
    hands_per_type["full_house"].append(key)
    if hand["cards"]["J"] == 1:
      hands_per_type_joker["4_of_a_kind"].append(key)
    elif hand["cards"]["J"] in [2,3]:
      hands_per_type_joker["5_of_a_kind"].append(key)
    else:
      hands_per_type_joker["full_house"].append(key)
  elif 3 in hand['multiples']:
    hands_per_type["triple"].append(key)
    if hand["cards"]["J"] == 2:
      hands_per_type_joker["5_of_a_kind"].append(key)
    elif hand["cards"]["J"] in [1, 3]:
      hands_per_type_joker["4_of_a_kind"].append(key)
    else:
      hands_per_type_joker["triple"].append(key)
  elif all([2 in hand['multiples'], hand['multiples'][2]==2]):
    hands_per_type["two_pair"].append(key)
    if hand["cards"]["J"] == 1:
      hands_per_type_joker["full_house"].append(key)
    elif hand["cards"]["J"] == 2:
      hands_per_type_joker["4_of_a_kind"].append(key)
    else:
      hands_per_type_joker["two_pair"].append(key)
  elif 2 in hand['multiples']:
    hands_per_type["one_pair"].append(key)
    if hand["cards"]["J"] in [1, 2]:
      hands_per_type_joker["triple"].append(key)
    else:
      hands_per_type_joker["one_pair"].append(key)
  else:
    hands_per_type["garbage"].append(key)
    if hand["cards"]["J"] == 1:
      hands_per_type_joker["one_pair"].append(key)
    else:
      hands_per_type_joker["garbage"].append(key)

final_order = []
final_order_joker = []
hand_order=["garbage","one_pair", "two_pair", "triple", "full_house", "4_of_a_kind", "5_of_a_kind"]
for win_type in hand_order:
  hands_per_type[win_type].sort(key=lambda x: hands[x]["score"])
  hands_per_type_joker[win_type].sort(key=lambda x: hands[x]["score_joker"])
  final_order += hands_per_type[win_type]
  final_order_joker += hands_per_type_joker[win_type]

#print(final_order)
total1 = 0
for i,hand in enumerate(final_order):
  total1 += (i+1)*hands[hand]['bet']

print(total1)
total2 = 0
for i,hand in enumerate(final_order_joker):
  total2 += (i+1)*hands[hand]['bet']
print(total2)
