import sys
from collections import defaultdict

with open(sys.argv[1]) as f:
    data = f.read().splitlines()

games = []
no_of_cards = defaultdict(lambda: 1)
for i, line in enumerate(data):
    winning, cards = line.split(": ")[1].split(" | ")
    games.append(
        {
            "win": [int(j) for j in winning.split()],
            "game": [int(k) for k in cards.split()],
        }
    )
total = 0
for i, game in enumerate(games):
    matches = 0
    for card in game["game"]:
        if card in game["win"]:
            matches += 1
    score = 2 ** (matches - 1) if matches > 0 else 0

    for j in range(i + 1, i + 1 + matches):
        no_of_cards[j] += no_of_cards[i]
    total += int(score)
print(total)
print(sum(no_of_cards.values()))
